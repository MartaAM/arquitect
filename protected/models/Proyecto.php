<?php

/**
 * This is the model class for table "proyecto".
 *
 * The followings are the available columns in table 'proyecto':
 * @property integer $id
 * @property string $nombre
 * @property string $descripcion
 * @property string $slug
 * @property integer $municipio_id
 * @property string $anyo
 * @property integer $superficie
 * @property integer $tipologia_id
 * @property integer $constructor
 * @property integer $promotor
 * @property string $colaboradores
 *
 * The followings are the available model relations:
 * @property Empresa-particular $constructor0
 * @property Municipio $municipio
 * @property Empresa-particular $promotor0
 * @property Tipologia $tipologia
 */
class Proyecto extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */

    public $idcomunidad;
    public $idprovincia;
    public $idmunicipio;

    public function tableName()
    {
        return 'proyecto';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre, descripcion, slug, municipio_id', 'required'),
            array('municipio_id, superficie, tipologia_id, constructor, promotor, gallery_id', 'numerical', 'integerOnly'=>true),
            array('nombre', 'length', 'max'=>100),
            array('slug', 'length', 'max'=>125),
            array('anyo', 'length', 'max'=>4),
            array('colaboradores', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nombre, descripcion, slug, municipio_id, anyo, superficie, tipologia_id, constructor, promotor, colaboradores, gallery_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'construct' => array(self::BELONGS_TO, 'Empresa', 'constructor'),
            'municipio' => array(self::BELONGS_TO, 'Municipio', 'municipio_id'),
            'promo' => array(self::BELONGS_TO, 'Empresa', 'promotor'),
            'tipologia' => array(self::BELONGS_TO, 'Tipologia', 'tipologia_id'),
            'gallery' => array(self::BELONGS_TO, 'Gallery', 'gallery_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'slug' => 'Slug',
            'municipio_id' => 'Municipio',
            'anyo' => 'Anyo',
            'superficie' => 'Superficie',
            'tipologia_id' => 'Tipologia',
            'constructor' => 'Constructor',
            'promotor' => 'Promotor',
            'colaboradores' => 'Colaboradores',
            'gallery_id' => 'Galeria',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('nombre',$this->nombre,true);
        $criteria->compare('descripcion',$this->descripcion,true);
        $criteria->compare('slug',$this->slug,true);
        $criteria->compare('municipio_id',$this->municipio_id);
        $criteria->compare('anyo',$this->anyo,true);
        $criteria->compare('superficie',$this->superficie);
        $criteria->compare('tipologia_id',$this->tipologia_id);
        $criteria->compare('constructor',$this->constructor);
        $criteria->compare('promotor',$this->promotor);
        $criteria->compare('colaboradores',$this->colaboradores,true);
        $criteria->compare('gallery_id',$this->gallery_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Proyecto the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getAnyo(){

        $anyoActual = date('Y');
        $desde = $anyoActual - 20; // Sobra material, si no habrá que cambiarlo
        $rango = range($anyoActual, $desde);

        return array_combine($rango, $rango);
    }

    public function getListadoTipos(){
        $tipologia=Tipologia::model()->findAll();
        return CHtml::listData($tipologia,"id","nombre");
    }

    public function getListadoEmpresas(){
        $empresa=Empresa::model()->findAll();
        return CHtml::listData($empresa,"id","nombre");
    }

    public function behaviors()
    {
        return array(
            'sluggable'=>array(
                'class'=>'ext.RSluggable',
                'atributo'=>'slug',
                'valor'=>'nombre'
            ),

            'galleryBehavior'=>array(
                'class'=>'GalleryBehavior',
                'idAttribute'=>'gallery_id',
                'versions'=>array(
                	'detalle'=>array(
                		'resize' =>array(300,NULL)
                	),
                    'completo'=>array(
                    	'resize'=>array(1150, NULL)
                    )
                ),
                'name'=>TRUE,
                'description'=>TRUE
            )
        );
    }

}
