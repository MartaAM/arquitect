<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Vicent Bertran | Arquitecto',
    'sourceLanguage'=>'es',
    'language'=>'es',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.extensions.image.*',
		'application.extensions.gallerymanager.*',
		'application.extensions.gallerymanager.models.*',
        'ext.chosen.Chosen',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
/*
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'admin',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
 */

	),

	// application components
	'components'=>array(

		'image'=>array(
		    'class'=>'application.extensions.image.CImageComponent',
		    'driver'=>'GD'
		),

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

        'clientScript' => array(
            'packages'=>array(
                'jquery'=>array(
                    'baseUrl'=>'http://ajax.googleapis.com/ajax/libs/jquery',
                    'js'=>array('1.9.1/jquery.min.js')
                ),
            )
        ),

		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
				/*'gii' => 'gii',
                'gii/<controller:\w+>' => 'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',
*/
                '/'=>'site/index',
                'proyectos'=>'site/proyectos',
                'proyecto/<id:\d+>-<slug:[\w\-]+>'=>'site/proyecto',
                '/<view:(trayectoria|sobre)>' => 'site/page',
                'contacto'=>'site/contact',
                'administrar'=>'proyecto/admin',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),


		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=arquitect',
			'emulatePrepare' => true,
			'username' => 'arquitect',
			'password' => 'pepito',
			'charset' => 'utf8',
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
                /*
				array(
					'class'=>'CWebLogRoute',
				),
                 */

			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
        'emailArquitecto'=>'vbertran@ctac.es',
	),
);
