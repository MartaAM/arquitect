<?php

class SiteController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Inicio';
        Yii::app()->clientScript->registerMetaTag('Vicent Bertran Arquitecto Castellón | Estas pensando en construir una nueva casa, nosotros te realizamos el proyecto arquitectónico','description');
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $this->pageTitle = 'Contacto';
        $model=new ContactForm;
        if(isset($_POST['ContactForm']))
        {
            $model->attributes=$_POST['ContactForm'];
            if($model->validate())
            {
                $name='=?UTF-8?B?'.base64_encode($model->name).'?=';
                $subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
                $headers="From: $name <{$model->email}>\r\n".
                    "Reply-To: {$model->email}\r\n".
                    "MIME-Version: 1.0\r\n".
                    "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['emailArquitecto'],$subject,$model->body,$headers);
                Yii::app()->user->setFlash('contact','Gracias por ponerse en contacto con nosotros');
                $this->refresh();
            }
        }
        $this->render('contact',array('model'=>$model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $model=new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login',array('model'=>$model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionProyectos($tipo = null)
    {
       $this->pageTitle = 'Proyectos';
        // Listamos todos los proyectos
        // $proyectos = Proyecto::model()->with(.....)->findAll();
        // Cargamos la vista y pasamos los proyectos
        // $this->render('vistaProyectos', array('proyectos'=>$proyectos));
        $tipologia = array();
        $relaciones = array( 'construct', 'promo', 'municipio');
        if ($tipo != null) {
            $tipologia['tipologia'] = array(
                'condition'=>'tipologia.nombre = :tipo',
                'params'=>array(':tipo'=>$tipo),
            );
        } else {
            $tipologia[] = 'tipologia';
        }

        $proyectos = Proyecto::model()->with(CMap::mergeArray($tipologia, $relaciones))->findAll();

        $this->render('vistaProyectos', array('proyectos'=>$proyectos));

    }

    public function actionProyecto($id,$slug){

    	$relaciones = array('construct', 'promo', 'municipio','tipologia','gallery');


        $criteria = new CDbCriteria;
        $criteria->condition = 't.id = :id and slug = :slug';
        $criteria->params = array(
            ':id'=>$id,
            ':slug'=>$slug,
        );
    	$proyecto = Proyecto::model()->with($relaciones)->find($criteria);
        if($proyecto == null)
            throw new CHttpException('404', 'El proyecto que busca no ha sido encontrado');

        $this->pageTitle = 'Proyecto - ' . $proyecto->nombre;
    	$this->render('vistaProyecto', array('proyecto'=>$proyecto));
    }

}
