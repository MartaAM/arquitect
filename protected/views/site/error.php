
<!-- 404 Intro Section -->
        <section class="full-screen-intro bg-img25 overlay-dark light-color parallax parallax-background2">
            <div class="content-cap-wraper">
                <div class="content-caption">
                    <h1 class="large mb-25">Error <?php echo $code; ?></h1>
                    <p class="lead content-wd650"><?php echo CHtml::encode($message); ?><br />Puedes consultar otras páginas.</p>
                    <br />
                    <a href="<?php echo $this->createUrl('site/index'); ?>" class="btn btn-md btn-white"><i class="fa fa-angle-left left"></i>Inicio</a>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <!-- End 404 Intro Section -->