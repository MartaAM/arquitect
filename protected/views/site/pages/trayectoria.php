<?php
Yii::app()->clientScript->registerMetaTag('Conoce la experiencia arquitectónica de Vicent Bertran Arquitecto Castellón','description');
?>
<?php $this->pageTitle = 'Trayectoria'; ?>
	<!-- Intro Section -->
        <section class="inner-intro bg-img3 overlay-light parallax parallax-background2">
            <div class="container">
                <div class="row title">
                    <h2 class="h2">Trayectoria</h2>
                    <div class="page-breadcrumb">
                        <a href="<?php echo $this->createUrl('site/index'); ?>">Inicio</a>/<span>Trayectoria</span>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
    <!-- End Intro Section -->

<!-- About Section -->
        <section class="ptb ptb-sm-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Trayectoria</h3>
                        <p class="lead">Vicent Bertran, apasionado del diseño arquitectónico, es arquitecto por la Escuela Superior de Arquitectura de Valencia (ETSAV) desde el 2007, año en el que creó su propio despacho tras diversas colaboraciones con distintos arquitectos durante su época de estudiante.</p>
                    </div>
                    <div class="col-md-6">
                        <p>Su trabajo destaca por las líneas puras, pero siempre buscando la optimización del espacio, además de la perfecta combinación de materiales.</p>
						<p>Desarrolla Proyectos de Arquitectura muy diversos: viviendas unifamiliares, reformas y acondicionamientos de viviendas, locales y oficinas.</p>
                    </div>
                </div>

            </div>
        </section>
