<?php
Yii::app()->clientScript->registerMetaTag('Para más información contacta con Vicent Bertran Arquitecto Castellón','description');
?>

	<!-- Intro Section -->
        <section class="inner-intro bg-img2 overlay-light parallax parallax-background2">
            <div class="container">
                <div class="row title">
                    <h2 class="h2">Contacto</h2>
                    <div class="page-breadcrumb">
                        <a href="<?php echo $this->createUrl('site/index'); ?>">Inicio</a>/<span>Contacto</span>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
    <!-- End Intro Section -->

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<!-- Contact Section -->
        <section class="ptb ptb-sm-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <h3>Contacto</h3>
                        <p class="lead">Si tienes cualquier pregunta, por favor rellena el siguiente formulario para ponerte en contacto con nosotros. Gracias.</p>
                    </div>
                </div>
                <div class="spacer-75"></div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <!-- Contact FORM -->

							<?php $form=$this->beginWidget('CActiveForm', array(
								'id'=>'contact-form',
								'enableClientValidation'=>true,
								'clientOptions'=>array(
									'validateOnSubmit'=>true,
								),
                                'htmlOptions'=>array(
                                    'class'=>'contact-form',
                                    'role'=>'form'
                                ),
							)); ?>

								<p class="note">Los campos con <span class="required">*</span> son obligatorios.</p>

								<?php echo $form->errorSummary($model); ?>

                            <div class="form-field-wrapper">
                                <?php echo $form->labelEx($model,'name'); ?>
                                <?php echo $form->textField($model,'name', array('class'=>'input-sm form-full', 'placeholder'=>$model->getAttributeLabel('name') )); ?>
                                <?php echo $form->error($model,'name'); ?>
                            </div>

                            <div class="form-field-wrapper">
                                <?php echo $form->labelEx($model,'email'); ?>
                                <?php echo $form->textField($model,'email', array('class'=>'input-sm form-full', 'placeholder'=>$model->getAttributeLabel('email') )); ?>
                                <?php echo $form->error($model,'email'); ?>
                            </div>

                            <div class="form-field-wrapper">
                                <?php echo $form->labelEx($model,'subject'); ?>
                                <?php echo $form->textField($model,'subject', array('class'=>'input-sm form-full', 'placeholder'=>$model->getAttributeLabel('subject') )); ?>
                                <?php echo $form->error($model,'subject'); ?>
                            </div>

                            <div class="form-field-wrapper">
                                <?php echo $form->labelEx($model,'body'); ?>
                                <?php echo $form->textArea($model,'body', array('class'=>'input-sm form-full', 'placeholder'=>$model->getAttributeLabel('body'), 'rows'=>6 )); ?>
                                <?php echo $form->error($model,'body'); ?>
                            </div>

                            <button class="btn btn-md btn-black form-full" type="submit" id="form-submit" name="submit">Enviar</button>
                        <?php $this->endWidget(); ?>
                        <!-- END Contact FORM -->
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact Section -->

		<!-- Map Section -->
        <section class="map">
            <div id="map"></div>
        </section>
        <!-- Map Section -->

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/map.js"></script>


<?php endif; ?>
