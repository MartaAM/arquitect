<?php
Yii::app()->clientScript->registerMetaTag('Proyectos de Viviendas de Vicent Bertran Arquitecto Castellón','description');
?>
	<!-- Intro Section -->
        <section class="inner-intro bg-img4 overlay-light parallax parallax-background2">
            <div class="container">
                <div class="row title">
                    <h2 class="h2">Proyectos</h2>
                    <div class="page-breadcrumb">
                        <a href="<?php echo $this->createUrl('site/index'); ?>">Inicio</a>/<span>Proyectos</span>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
    <!-- End Intro Section -->

    <!-- Work Detail Section -->
        <section class="ptb ptb-sm-80">
            <div class="container">
                <!-- work Filter -->
                <div class="row">
                    <ul class="container-filter categories-filter">
                        <li><a class="categories active" data-filter="*">Todo</a></li>
                        <li><a class="categories" data-filter=".residencial">Residencial</a></li>
                        <li><a class="categories" data-filter=".rehabilitacion">Rehabilitación</a></li>
                        <li><a class="categories" data-filter=".publico">Público</a></li>
                    </ul>
                </div>
                <!-- End work Filter -->
                <div class="container-masonry nf-col-4">
					<?php foreach($proyectos as $proyecto): ?>
                        <div class="nf-item <?php echo strtolower($proyecto->tipologia->nombre); ?>">
                        <?php $foto=$proyecto->galleryBehavior->getPhoto(); ?></br>
	                        <div class="item-box">
	                            <a href="<?php echo Yii::app()->createUrl('site/proyecto',array('id'=>$proyecto->id,'slug'=>$proyecto->slug)); ?>">
                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/gallery/_<?php echo $foto->rank ?>.jpg" class="item-container">
	                                <div class="item-mask">
	                                    <div class="item-caption">
	                                        <h6 class="white"><?php echo $proyecto->nombre; ?></h6>
	                                    </div>
	                                </div>
	                            </a>
	                        </div>
	                    </div>
					<?php endforeach; ?>
                </div>

            </div>

        </section>
        <!-- End Work Detail Section -->





