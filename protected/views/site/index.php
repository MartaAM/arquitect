<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
        <!-- Intro Section -->
        <section class="hero">

            <!-- Hero Slider Section -->
            <div class="flexslider fullscreen-carousel hero-slider-1 parallax parallax-section1">
                <ul class="slides">
                    <li>
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/full/intro1.jpg" alt="portfolio" />
                        <div class="overlay-hero overlay-light">
                            <div class="container caption-hero dark-color">
                                <div class="inner-caption">

                                    <h2 class="h2">Vicent Bertran | Arquitecto</h2>
                                    <p class="lead">Pasión por la arquitectura</p>

                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/full/intro2.jpg" alt="vivienda" draggable="false" />
                        <div class="overlay-hero overlay-dark">
                            <div class="container caption-hero light-color">
                                <div class="inner-caption">
                                    <h2 class="h2">Vicent Bertran | Arquitecto</h2>
                                    <p class="lead">Viviendas para vivirlas</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/full/intro3.jpg" alt="Photography" draggable="false" />

                        <div class="overlay-hero overlay-light">
                            <div class="container caption-hero dark-color">
                                <div class="inner-caption">
                                    <h2 class="h2">Vicent Bertran | Arquitecto</h2>
                                    <p class="lead">Diseño y modernidad</p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- End Hero Slider Section -->
        </section>
        <div class="clearfix"></div>
        <!-- End Intro Section -->
        
        <!-- Icon Box Section -->
        <section class="ptb ptb-80">
            <div class="container text-center">
                <h3>Filosofía</h3>
                <div class="spacer-60"></div>
                <div class="row">
                    <div class="col-md-4 col-sm-6 mb-45">
                        <div class="page-icon-top"><i class="ion ion-ios-heart-outline"></i></div>
                        <h5>Pasión</h5>
                        <p>Pasión y Arquitectura siempre van de la mano, y eso se nota en todos nuestros proyectos que están llenos de emociones.</p>
                        <p>"De un trazo nace la arquitectura"— <i>Oscar Niemeyer</i></p>
                    </div>
                    <div class="col-md-4 col-sm-6 mb-45">
                        <div class="page-icon-top"><i class="ion ion-ios-home-outline"></i></div>
                        <h5>Hogar</h5>
                        <p>Creamos viviendas para vivirlas, para crear nuestro refugio personal. Para que cada día disfrutemos de nuestro hogar.</p>
                        <p>"La casa debe ser el estuche de la vida, la máquina de felicidad"— <i>Le Corbusier</i></p>
                    </div>
                    <div class="col-md-4 col-sm-6 mb-45">
                        <div class="page-icon-top"><i class="ion ion-ios-color-filter-outline"></i></div>
                        <h5>Modernidad</h5>
                        <p>Viviendas modernas, de líneas puras y sencillas.</p>
                        <p>"El arquitecto es el hombre sintético, el que es capaz de ver las cosas en conjunto antes de que estén hechas"— <i>Antonio Gaudí</i></p>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Icon Box Section -->


