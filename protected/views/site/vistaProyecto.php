<?php
Yii::app()->clientScript->registerMetaTag($proyecto->descripcion,'description');
?>

	<!-- Intro Section -->
        <section class="inner-intro inner-intro-small bg-img5 light-color overlay-dark parallax parallax-background2">
            <div class="container">
                <div class="row title">
                    <h2 class="h2">Proyectos | <strong><?php echo $proyecto->nombre; ?></strong></h2>
                    <div class="page-breadcrumb">
                        <a href="<?php echo $this->createUrl('site/index'); ?>">Inicio</a>/<a href="<?php echo $this->createUrl('site/proyectos'); ?>">Proyectos</a>/<span><?php echo $proyecto->nombre; ?></span>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
    <!-- End Intro Section -->

    <!-- Work Detail Section -->
        <section class="pt pt-sm-80">
            <div class="container">

                <div class="row mb-60 mb-xs-30">
                    <div class="col-md-6">
                        <h4><?php echo $proyecto->nombre; ?></h4>
                    </div>
                    <div class="col-md-6">
                        <p class="lead">
                            <?php echo $proyecto->descripcion; ?>
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="owl-carousel image-slider o-flow-hidden">
							<?php foreach ($proyecto->galleryBehavior->getGalleryPhotos() as $photo) : ?>
                                <div class="item">
                                     <?php echo CHtml::image(Yii::app()->request->baseUrl.'/gallery/'.$photo->rank.'.jpg', $photo->name); ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>

                <div class="row mtb-60 mtb-xs-30">
                    <div class="col-md-6 mb-30">
                        <div class="project-detail-block">
                            <p>
                                <strong class="dark-color">Lugar :</strong><?php echo $proyecto->municipio->nombre; ?> (<?php echo $proyecto->municipio->provincia->nombre; ?>)
                            </p>

							<?php if ($proyecto->anyo != null){ ?>
	                            <p>
	                                <strong class="dark-color">Año :</strong><?php echo $proyecto->anyo; ?>
	                            </p>
                            <?php } ?>
                            <?php if ($proyecto->superficie != null){ ?>
	                            <p>
	                                <strong class="dark-color">Superficie :</strong><?php echo $proyecto->superficie; ?> m2
	                            </p>
							<?php } ?>
							<p>
                                <strong class="dark-color">Tipologia :</strong><?php echo $proyecto->tipologia->nombre; ?>
                            </p>
	                        <?php if ($proyecto->constructor != null){ ?>
                            <p>
                                <strong class="dark-color">Constructor :</strong><?php echo $proyecto->construct->nombre; ?>
                            </p>
                            <?php } ?>
                            <?php if ($proyecto->promotor != null){ ?>
                            <p>
                                <strong class="dark-color">Promotor :</strong><?php echo $proyecto->promo->nombre; ?>
                            </p>
                            <?php } ?>
                            <?php if ($proyecto->colaboradores != null){ ?>
                            <p>
                                <strong class="dark-color">Colaboradores :</strong><?php echo $proyecto->colaboradores; ?>
                            </p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <a class="btn btn-lg btn-black" href="<?php echo $this->createUrl('site/proyectos'); ?>"><i class="fa fa-angle-double-left"></i> Volver a Proyectos</a>
            </div>

        </section>
        <!-- End Work Detail Section -->
   

