<!-- Intro Section -->
        <section class="inner-intro bg-img2 overlay-light parallax parallax-background2">
            <div class="container">
                <div class="row title">
                    <h2 class="h2">Login</h2>
                    <div class="page-breadcrumb">
                        <a href="<?php echo $this->createUrl('site/index'); ?>">Inicio</a>/<span>Login</span>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
    <!-- End Intro Section -->



<?php if(Yii::app()->user->hasFlash('login')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('login'); ?>
</div>

<?php else: ?>

<!-- Login Section -->
        <section class="ptb ptb-sm-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <h3>Iniciar Sesión</h3>
                    </div>
                </div>
                <div class="spacer-75"></div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <!-- Login FORM -->


						<?php $form=$this->beginWidget('CActiveForm', array(
								'id'=>'login-form',
								'enableClientValidation'=>true,
								'clientOptions'=>array(
									'validateOnSubmit'=>true,
								),
								'htmlOptions'=>array(
                                    'class'=>'login-form',
                                    'role'=>'form'
                                ),
						)); ?>

							<?php echo $form->errorSummary($model); ?>
	<div class="form-field-wrapper">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username', array('class'=>'input-sm form-full', 'placeholder'=>$model->getAttributeLabel('username') )); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="form-field-wrapper">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password', array('class'=>'input-sm form-full', 'placeholder'=>$model->getAttributeLabel('password'))); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="form-field-wrapper rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

		<?php echo CHtml::submitButton('Login', array('class'=>'btn btn-md btn-black form-full')); ?>

                        <?php $this->endWidget(); ?>
                        <!-- END Login FORM -->
                    </div>
                </div>
            </div>
        </section>
        <!-- Login Section -->

<?php endif; ?>

