<?php
/* @var $this ComunidadController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Comunidads',
);

$this->menu=array(
	array('label'=>'Create Comunidad', 'url'=>array('create')),
	array('label'=>'Manage Comunidad', 'url'=>array('admin')),
);
?>

<h1>Comunidads</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
