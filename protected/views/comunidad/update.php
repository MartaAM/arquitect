<?php
/* @var $this ComunidadController */
/* @var $model Comunidad */

$this->breadcrumbs=array(
	'Comunidads'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Comunidad', 'url'=>array('index')),
	array('label'=>'Create Comunidad', 'url'=>array('create')),
	array('label'=>'View Comunidad', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Comunidad', 'url'=>array('admin')),
);
?>

<h1>Update Comunidad <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>