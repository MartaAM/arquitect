<?php
/* @var $this ProyectoController */
/* @var $model Proyecto */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'descripcion'); ?>
		<?php echo $form->textArea($model,'descripcion',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'slug'); ?>
		<?php echo $form->textField($model,'slug',array('size'=>60,'maxlength'=>125)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'municipio_id'); ?>
		<?php echo $form->textField($model,'municipio_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'anyo'); ?>
		<?php echo $form->textField($model,'anyo',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'superficie'); ?>
		<?php echo $form->textField($model,'superficie'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipologia_id'); ?>
		<?php echo $form->textField($model,'tipologia_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'constructor'); ?>
		<?php echo $form->textField($model,'constructor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'promotor'); ?>
		<?php echo $form->textField($model,'promotor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'colaboradores'); ?>
		<?php echo $form->textArea($model,'colaboradores',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->