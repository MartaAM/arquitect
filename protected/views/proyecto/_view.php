<?php
/* @var $this ProyectoController */
/* @var $data Proyecto */
?>

<div class="view">
	
	<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/<?php echo CHtml::encode($data->id); ?>.jpg" alt="Imagen <?php echo CHtml::encode($data->nombre); ?>">
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />
	
	<?php if ($data->municipio_id != null){ ?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('municipio')); ?>:</b>
		<?php $municipio=Municipio::model()->findByPK($data->municipio_id); ?>
		<?php echo CHtml::encode($municipio->nombre); ?>
		<br />
	<?php } ?>

	<?php if ($data->anyo != null){ ?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('anyo')); ?>:</b>
		<?php echo CHtml::encode($data->anyo); ?>
		<br />
	<?php } ?>

	<?php if ($data->superficie != null){ ?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('superficie')); ?>:</b>
		<?php echo CHtml::encode($data->superficie); ?> m2
		<br />
	<?php } ?>

	<?php if ($data->tipologia_id != null){ ?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('tipologia')); ?>:</b>
		<?php $tipologia=Tipologia::model()->findByPK($data->tipologia_id); ?>
		<?php echo CHtml::encode($tipologia->nombre); ?>
		<br />
	<?php } ?>
	
	<?php if ($data->constructor != null){ ?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('constructor')); ?>:</b>
		<?php $constructor=Empresa::model()->findByPK($data->constructor); ?>
		<?php echo CHtml::encode($constructor->nombre); ?>
		<br />
	<?php } ?>

	<?php if ($data->promotor != null){ ?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('promotor')); ?>:</b>
		<?php $promotor=Empresa::model()->findByPK($data->promotor); ?>
		<?php echo CHtml::encode($promotor->nombre); ?>
		<br />
	<?php } ?>
	
	<?php if ($data->colaboradores != null){ ?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('colaboradores')); ?>:</b>
		<?php echo CHtml::encode($data->colaboradores); ?>
		<br />
	<?php } ?>

	

</div>