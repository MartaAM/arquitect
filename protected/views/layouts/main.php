<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<!-- Favicone Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico">
    <link rel="icon" type="image/png" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png">
    <link rel="apple-touch-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png">

	<!-- CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ionicons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/plugin/jPushMenu.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/plugin/animate.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/navigation.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.css" rel="stylesheet" type="text/css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body class="full-intro">

	<!-- Script Facebook -->
	<div id="fb-root"></div>
		<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.5";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
	<!-- End Script Facebook -->

	<!-- Preloader -->
    <section id="preloader">
        <div class="loader" id="loader">
            <div class="loader-img"></div>
        </div>
    </section>
    <!-- End Preloader -->

	<!-- Site Wraper -->
    <div class="wrapper">

        <!-- HEADER -->
        <header class="header">
            <div class="container">

                <!-- logo -->
                <div class="logo">
                    <a href="<?php echo $this->createUrl('site/index'); ?>">
                        <img class="l-black" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" />
                        <img class="l-white" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" />
                        <img class="l-color" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" />
                    </a>
                </div>
                <!--End logo-->

                <!-- Navigation Menu -->
                <nav class='navigation'>
                    <ul>
                        <li>
                            <a href="<?php echo $this->createUrl('site/index'); ?>">Inicio</a>
                        </li>

                        <li>
                            <a href="<?php echo $this->createUrl('site/page', array('view'=>'trayectoria')); ?>">Trayectoria</a>
                        </li>

                        <li>
                            <a href="<?php echo $this->createUrl('site/proyectos'); ?>">Proyectos</a>
                            <!-- Nav Dropdown
                            <ul class="nav-dropdown">
                                <li><a href="<?php //echo Yii::app()->request->baseUrl; ?>/site/proyectos/tipo/Residencial">Residencial</a></li>
                                <li><a href="<?php //echo Yii::app()->request->baseUrl; ?>/site/proyectos/tipo/Rehabilitacion">Rehabilitación</a></li>
                                <li><a href="<?php //echo Yii::app()->request->baseUrl; ?>/site/proyectos/tipo/Publico">Público</a></li>
                            </ul>
                            End Nav Dropdown -->
                        </li>
                        <li>
                            <a href="<?php echo $this->createUrl('site/contact'); ?>">Contacto</a>
                        </li>
                    </ul>
                </nav>
                <!--End Navigation Menu -->

            </div>
        </header>
        <!-- END HEADER -->

        <!-- CONTENT --------------------------------------------------------------------------------->



		<!-- Section -->

                    <?php echo $content; ?>

        <!-- End Section -->

	    <!-- END CONTENT ---------------------------------------------------------------------------->

		<!-- FOOTER -->
        <footer class="footer pt-15">
            <div class="container">
                <div class="row mb-15">
                    <!-- Logo -->
                    <div class="col-md-3 col-sm-3 col-xs-12 mb-xs-30">
                        <a class="footer-logo" href="home.html">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" /></a>
                    </div>
                </div>
                <!--Footer Info -->
                <div class="row footer-info mb-0">
                    <div class="col-md-3 col-sm-12 col-xs-12 mb-sm-30">
                    	<h5>Trayectoria</h5>
                        <p class="mb-xs-0">Vicent Bertran apasionado del diseño arquitectónico cursó sus estudios en la Escuela Técnica Superior de Arquitectura de Valencia.</p>
                        <a class="btn-link-a" href="<?php echo $this->createUrl('site/page', array('view'=>'trayectoria')); ?>">Leer Más</a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 mb-sm-30 mb-xs-0">
                        <h5>Ultimos proyectos</h5>
                        <ul class="link">
                        	<?php $ultProyecto = Proyecto::model()->findAll(array('limit'=>3,'order'=>'id DESC')); ?>
                        	<?php foreach ($ultProyecto as $value) { ?>
                            	<li>
                            		<a href="<?php echo Yii::app()->createUrl('site/proyecto',array('id'=>$value->id,'slug'=>$value->slug)); ?>">
                    				<?php echo $value->nombre;?>
                  					</a>
                  					<p><?php echo $value->descripcion;?></p>
                  				</li>
                            <?php } ?>
                        </ul>
                        </br>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                    	<h5>Contacto</h5>
	                        <p>Atzeneta del Maestrat</p>
	                        <p>Vall d'Alba</p>
	                        <p>Castellón</p>
	                        <ul class="link-small">
	                            <li><a href="mailto:vbertran@ctac.es"><i class="fa fa-envelope-o left"></i>vbertran@ctac.es</a></li>
	                            <li><a><i class="fa fa-phone left"></i>655 54 09 47</a></li>
	                            <li><a target="_blank" href="https://www.facebook.com/Vicent-Bertran-Arquitecto-553019691380179"><i class="fa fa-facebook"></i> Vicent Bertran Arquitecto</a></li>
	                        </ul>
	                        </br>
                    </div>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<div class="fb-page" data-href="https://www.facebook.com/Vicent-Bertran-Arquitecto-553019691380179/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
					</div>

                </div>
                <!-- End Footer Info -->
            </div>

            <hr />

            <!-- Copyright Bar -->
            <section class="copyright ptb-15 pt-0">
                <div class="container">
                    <p class="">
                    	Vicent Bertran | Arquitecto - Copyright &copy; <?php echo date('Y'); ?><br/>
                    	Desarollado por <a target="_blank" href="http://www.essenciadisseny.es/">Essència Disseny</a><br/>
                    </p>
                </div>
            </section>
            <!-- End Copyright Bar -->

        </footer>
        <!-- END FOOTER -->

		<!-- Scroll Top -->
	        <a class="scroll-top">
	            <i class="fa fa-angle-double-up"></i>
	        </a>
	        <!-- End Scroll Top -->

	</div>
	<!-- Site Wraper End -->


	<!-- JS -->

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.2.min.js" type="text/javascript"></script>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/jquery.easing.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/jquery.flexslider.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/background-check.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/jquery.fitvids.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/jquery.viewportchecker.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/jquery.stellar.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/wow.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/jquery.colorbox-min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/owl.carousel.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/isotope.pkgd.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/imagesloaded.pkgd.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/jPushMenu.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/jquery.fs.tipper.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugin/mediaelement-and-player.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/theme.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/navigation.js" type="text/javascript"></script>


</body>
</html>
