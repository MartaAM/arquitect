<?php

class RSluggable extends CActiveRecordBehavior
{
    public $atributo;
    public $valor;

    public function beforeValidate($event)
    {
        if($event->isValid)
            $this->getOwner()->{$this->atributo} = self::seoUrl($this->getOwner()->{$this->valor});

        parent::beforeValidate($event);
    }

    private static function seoUrl($cadena)
    {
        //Minúsculas todo
        $cadena = strtolower($cadena);
        //Eliminar acentos
        $cadena = strtr($cadena, array('á'=>'a', 'é'=>'e', 'í'=>'i','ó'=>'o','ú'=>'u', 'ñ'=>'n'));
        //Sólo caracteres alfanuméricos
        $cadena = preg_replace("/[^a-z0-9_\s-]/", "", $cadena);
        //Eliminar espacios o guiones múltiples
        $cadena = preg_replace("/[\s-]+/", " ", $cadena);
        //Pasar espacios y guiones bajos a guiones
        $cadena = preg_replace("/[\s_]/", "-", $cadena);

        return $cadena;
    }
}
